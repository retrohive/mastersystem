# master system

based on https://archive.org/details/no-intro_romsets (20210216-223458)

ROM not added :
```
❯ tree -hs --dirsfirst
.
├── [106K]  Battle Wings (World) (Sample).zip
├── [ 77K]  [BIOS] Alex Kidd in Miracle World (Korea) (Unl).zip
├── [ 79K]  [BIOS] Alex Kidd in Miracle World (USA, Europe).zip
├── [ 59K]  [BIOS] Hang On & Safari Hunt (USA, Europe, Brazil) (v2.4).zip
├── [ 28K]  [BIOS] Hang On (USA, Europe) (v3.4).zip
├── [ 79K]  [BIOS] Missile Defense 3-D (USA, Europe) (v4.4).zip
├── [6.6K]  [BIOS] Sega Master System (Europe) (v2.0).zip
├── [5.9K]  [BIOS] Sega Master System (Japan) (v2.1).zip
├── [6.6K]  [BIOS] Sega Master System (USA, Europe) (v1.3).zip
├── [2.4K]  [BIOS] Sega Master System (USA) (M404) (Proto).zip
├── [7.8K]  [BIOS] Sega Master System (USA) (Store Display Unit).zip
├── [3.7K]  [BIOS] Sega Master System (USA) (v1.0) (Proto).zip
├── [144K]  [BIOS] Sonic The Hedgehog (Europe).zip
├── [124K]  Game de Check! Koutsuu Anzen (Japan) (Proto).zip
├── [312K]  Phantasy Star (Japan) (Virtual Console).zip
├── [ 16K]  Puzzle (Korea) (Unl).zip
├── [ 18K]  Sagak-ui Bimil (Korea) (Unl).zip
└── [103K]  Slap Shoot (USA) (Beta).zip

0 directories, 18 files
```